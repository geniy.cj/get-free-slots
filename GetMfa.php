<?php

function GetResponseMfa(): string|null
{
    $date = date('Y-m-d');
    $consulates = getenv('CONSULATES');
    $serviceId = getenv('SERVICE_ID');
    $curl = curl_init();
    $answerText = "";
    curl_setopt_array($curl, array(
        CURLOPT_URL => getenv('WEBSITE_MFA') .'queue/consulates/'. $consulates . '/schedule?date=' . $date . '&dateEnd=' . $date . '&serviceId=' . $serviceId,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_POSTFIELDS => '{
	"apiKey": "9adcbaecaded0ffa48eabc6e36149a2d",
	"modelName": "Address",
	"calledMethod": "getSettlements"
}',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . GlobalClass::get('AuthToken'),
            'Content-Type: application/json',
            'Cookie: __cf_bm=NGB7LlHYEoqPjif3hMoma_5sFnbFfI3UlO_Ndq7skps-1665516044-0-ARXYzon5Kl+PaLcXlcEMqk6CzJKOPXrG6B6ZpFc0yxIan7zYqkOc/rKEazsTR2P5WQv1+YWd94vjV5b9R9BGSY58Mb9e8Sgg6aBrBZOfUyHX'
        ),
    ));

    $response = curl_exec($curl);
    Settype($response, "string");
    echo '<br>';
    echo GlobalClass::get('AuthToken') . '<br>';
    echo '<br>';
    echo var_dump($response) . '<br>';
    if ($response === 'Unauthorized' or $response === 'NULL' or empty($response)) {
        GlobalClass::add('gResponse', GetGResponse());
        GlobalClass::add('AuthToken', GetAuthToken());
        if (date('H') >= 8 && date('H') <= 20 ) {
            SendTelegramm("Get new token");
        }
        $answerText = GetResponseMfa();
    } else {
        curl_close($curl);
        $answer = json_decode($response);

        if (is_array($answer)) {
            foreach ($answer as $dates) {
                foreach ($dates as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $time) {
                            $answerText .= "$time->to <==> $time->from\n";
                        }
                    } else {
                        $answerText .= "$key ($value)\n";
                    }
                }
            }
        }
        if (!empty($answerText)) {
            SendTelegramm($answerText);
            GlobalClass::add('FreeSlots', true);
        }
    }
    return $answerText;
}
