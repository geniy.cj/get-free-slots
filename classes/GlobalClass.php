<?php
namespace GetSchedule\Classes;
class GlobalClass
{
    static $_stack = array();

    static function get($key)
    {
        return (isset(self::$_stack[$key])) ? self::$_stack[$key] : null;
    }

    static function add($key, $value = null)
    {
        self::$_stack[$key] = $value;
    }
}
