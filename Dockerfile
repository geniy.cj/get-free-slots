FROM php:8.0.2-cli

ADD crontab /etc/cron.d/hello-cron
RUN chmod 0644 /etc/cron.d/hello-cron

RUN touch /var/log/cron.log
# copy crontabs for root user

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
# installing cron package
RUN apt-get update && apt-get -y install cron

# start crond with log level 8 in foreground, output to stderr
CMD cron && tail -f /var/log/cron.log
