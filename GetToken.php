<?php
include("anticaptcha-php/anticaptcha.php");
include("anticaptcha-php/recaptchaV2proxyless.php");

function GetGResponse():String
{
    $api = new RecaptchaV2Proxyless();
    $api->setVerboseMode(true);

//your anti-captcha.com account key
    $api->setKey(getenv('KEY_RECAPTCHA'));

//target website address
    $api->setWebsiteURL(getenv('WEBSITE_URL'));

//recaptcha key from target website
    $api->setWebsiteKey(getenv('WEBSITE_KEY'));

//optional custom parameter which Google made for their search page Recaptcha v2
//$api->setDataSValue("'data-s' token from Google Search");

//Specify softId to earn 10% commission with your app.
//Get your softId here: https://anti-captcha.com/clients/tools/devcenter
    $api->setSoftId(0);

//create task in API
    if (!$api->createTask()) {
        $api->debout("API v2 send failed - " . $api->getErrorMessage(), "red");
        return false;
    }

    $taskId = $api->getTaskId();

    $gResponse='';
//wait in a loop for max 300 seconds till task is solved
    if (!$api->waitForResult(300)) {
//        echo "could not solve captcha\n";
//        echo $api->getErrorMessage() . "\n";
    } else {
        $gResponse = $api->getTaskSolution();
//        echo "\n";
//        echo "your recaptcha token: $gResponse\n\n";
    }
    return $gResponse;
}


function GetAuthToken(){

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => getenv('WEBSITE_MFA').'auth/session',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
    "countryId":"'.getenv('COUNTRY_ID').'",
    "g-recaptcha-response":"'.GlobalClass::get('gResponse').'"}',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response);

    return $result?->token ?? '';
}
