<?php
function sendTelegramm($text)
{
    if (getenv('USE_NOTIFICATION_TELEGRAM')) {
        time_sleep_until(microtime(true) + GlobalClass::get('DelayNotification'));
        $textShort = urlencode(mb_strimwidth($text, 0, 703));
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . getenv('ACCESS_TOKEN') . '/sendMessage?chat_id=' . getenv(
                    'CHAT_ID'
                ) . '&text=' . $textShort,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    }
}
