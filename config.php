<?php
error_reporting(E_ALL);
require_once __DIR__ . DIRECTORY_SEPARATOR  . "SendNotification.php";
require_once __DIR__ . DIRECTORY_SEPARATOR  . "GetMfa.php";
require_once __DIR__ . DIRECTORY_SEPARATOR  . "GetToken.php";
Use GetSchedule\Classes\DotEnv;

// Loading for .env at the root directory
(new DotEnv(__DIR__ . '/.env'))->load();


spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'GetSchedule\\Classes\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/classes/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

